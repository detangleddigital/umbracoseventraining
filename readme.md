# Gems Cafe

Gems Cafe is a simple website running on an Umbraco V7 CMS.

The goal of the project is to showcase what's possible with Umbraco V7, Visual Studio and a splash of PowerShell without going crazy with .NET. There's no use of heavyweight MVC, dynamics, or IoC. There's no reason why any of that couldn't be added though.

If you'd like to take this approach one step further and dynamically populate your view models then take a look at the [uHydrator project](http://blog.detangled-digital.com/post/introducing-uhydrator-for-umbraco), Bitbucket [here](https://bitbucket.org/chrisgaskell/uhydrator).

Get in touch [@CGaskell](https://twitter.com/CGaskell), [Detangled-Digital.com](http://detangled-digital.com/)

Note: The example uses an embedded SQL Server Compact database and has media items under version control - this is to enable the project to be distributed. You may not want to version control these items in your own solution.

## What's to see?

### Umbraco Functionality
* Route Hijacking
* Surface controller to render common components through child actions
* Full MVC implementation with true view models, no CMS logic in views
* Contact form with full model validation
* Base controller with CreateViewModel<T> to populate common fields
* Concept of view model inheritance to match doctype inheritance
* Recursive Properties
* Umbraco 7 child item listings in the back office
* Configured 404 page
* ImageResizer.net and full HTML helper support
* Donut caching [https://github.com/moonpyk/mvcdonutcaching](https://github.com/moonpyk/mvcdonutcaching) integrated and configured to clear on publish. Uses latest content service.

### Development Environment
* Umbraco V7
* Installation of CMS via Nuget
* .NET 4.5, Visual Studio 2013
* Two-stage web config transforms
* PowerShell bootstrap

### Two-stage Umbraco config transform
The two-stage config transform is something I've found useful for retaining the original Umbraco config, adding your global and then environment specific transforms. This makes upgrading really easy and allows you to keep all your global transforms for the project in one place.

It hooks into the AfterBuild target in the website project and runs the MSBuild 'TransformUmbracoConfigs' target in the .umbraco/Umbraco.targets file. Essentially this then:

* Takes ~/umbraco.web.config and applies the transform ~/web.global.config to create the file web.temp.config
* Then applies the transform web.[configuration].config to web.global.config to create the web.config file


## Site Setup

* Checkout the master branch
* Build the solution inside Visual Studio
* Run PowerShell as an administrator. Run 'Set-ExecutionPolicy RemoteSigned'
* Run the '.\install.ps1' PowerShell script. This will setup IIS, disk permissions and update the hosts file
* Navigate to [http://gemscafe.local/](http://gemscafe.local/)

_Note: If you run PowerShell scripts in windows 8 you'll need to set PowerShell to version 2 by opening PowerShell (run as admin) then executing 'powershell -version 2.0'_

## Umbraco 

### URLs

*  [http://gemscafe.local/](http://gemscafe.local/) - homepage.
*  [http://gemscafe.local/umbraco/](http://gemscafe.local/umbraco/) - CMS Backoffice.

### CMS User Account
* User: Admin           
* Pass: admin