﻿namespace GemsCafe.Website.ViewModels
{
    using GemsCafe.Website.ViewModels.Base;

    public class HomeViewModel : BaseViewModel
    {
        public string TitleIntro { get; set; }
        public string Title { get; set; }
        public string TagLine { get; set; }

        public object Content1Title { get; set; }
        public object Content1Text { get; set; }
        public object Content2Title { get; set; }
        public object Content2Text { get; set; }
        
    }
}