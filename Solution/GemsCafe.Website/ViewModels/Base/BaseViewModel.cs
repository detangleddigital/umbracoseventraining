﻿namespace GemsCafe.Website.ViewModels.Base
{
    using System;

    public abstract class BaseViewModel : IBaseViewModel
    {
        public string PageTitle { get; set; }
        public string PageDescription { get; set; }
        public DateTime PageConstructedAt { get; set; }
    }
}