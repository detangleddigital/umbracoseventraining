﻿namespace GemsCafe.Website.ViewModels
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using GemsCafe.Website.ViewModels.Base;

    public class ContactViewModel : BaseViewModel
    {
        public string Title { get; set; }
        public string GoogleMapsUrl { get; set; }
        public string Address { get; set; }
        public string FormIntro { get; set; }


        // Properties to support the form
        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter an email address")]
        [EmailAddress(ErrorMessage = "Please use a valid email address")]
        [DisplayName("Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a message")]
        public string Message { get; set; }

        public bool FormPostSuccess { get; set; }
    }
}