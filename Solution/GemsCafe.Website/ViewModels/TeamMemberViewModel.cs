﻿namespace GemsCafe.Website.ViewModels
{
    using GemsCafe.Website.App.Models;

    public class TeamMemberViewModel
    {
        public string Name { get; set; }
        public string JobTitle { get; set; }
        public UmbracoImage Image { get; set; }
    }
}