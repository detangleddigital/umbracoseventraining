﻿namespace GemsCafe.Website.App.CmsDefinitions
{
    public static class HomeDoctype
    {
        public static class Fields
        {
            public const string TitleIntro = "home_titleintro";
            public const string Title = "home_title";
            public const string TagLine = "home_tagline";

            public const string Content1Title = "home_contentblock1_title";
            public const string Content1Text = "home_contentblock1_text";
            public const string Content2Title = "home_contentblock2_title";
            public const string Content2Text = "home_contentblock2_text";
        }
    }
}