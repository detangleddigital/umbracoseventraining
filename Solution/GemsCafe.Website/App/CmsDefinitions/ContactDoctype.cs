﻿namespace GemsCafe.Website.App.CmsDefinitions
{
    public static class ContactDoctype
    {
        public static class Fields
        {
            public const string Title = "contact_title";
            public const string GoogleMapsUrl = "contact_googlemapsurl";
            public const string Address = "contact_address";
            public const string FormIntro = "contact_formintro";
        }
    }
}