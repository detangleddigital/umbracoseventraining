﻿namespace GemsCafe.Website.App.CmsDefinitions
{
    public static class AboutDoctype
    {
        public static class Fields
        {
            public const string Title = "about_title";
            public const string IntroImage = "about_introimage";
            public const string Intro = "about_intro";
            public const string TeamTitle = "about_teamtitle";
            
        }
    }
}