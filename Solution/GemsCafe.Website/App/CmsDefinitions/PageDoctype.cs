﻿namespace GemsCafe.Website.App.CmsDefinitions
{
    public static class PageDoctype
    {
        public static class Fields
        {
            public const string HeaderTitle = "header_title";
            public const string HeaderAddress = "header_address";

            public const string PageTitle = "page_title";
            public const string PageDescription = "page_description";

            public const string FooterText = "footer_text";
        }
    }
}