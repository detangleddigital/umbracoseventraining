﻿namespace GemsCafe.Website.Controllers
{
    using System.Web.Mvc;
    using DevTrends.MvcDonutCaching;
    using GemsCafe.Website.App;
    using GemsCafe.Website.App.CmsDefinitions;
    using GemsCafe.Website.ViewModels;
    using Umbraco.Web;
    using Umbraco.Web.Models;

    public class ContactController : BaseController
    {
        [DonutOutputCache(CacheProfile = CacheProfiles.SixHours)]
        [HttpGet]
        public override ActionResult Index(RenderModel model)
        {
            var viewModel = CreateViewModel<ContactViewModel>();
            PopulateViewModel(viewModel);
            return CurrentTemplate(viewModel);
        }

        /*
         * The antiforgerytoken is a way of ensuring that the request that is coming to a post action is actually one that originated from your 
         * outputted view. It stops cross site scripting attacks and handles post replay attacks too.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel model)
        {
            PopulateViewModel(model);

            if (!ModelState.IsValid)
            {
                return CurrentTemplate(model);
            }

            // 
            // Persist the model to a database or similar here....
            //

            model.FormPostSuccess = true;
            return CurrentTemplate(model);
        }

        private void PopulateViewModel(ContactViewModel viewModel)
        {
            viewModel.Title = CurrentPage.GetPropertyValue<string>(ContactDoctype.Fields.Title);
            viewModel.GoogleMapsUrl = CurrentPage.GetPropertyValue<string>(ContactDoctype.Fields.GoogleMapsUrl);
            viewModel.Address = CurrentPage.GetPropertyValue<string>(ContactDoctype.Fields.Address);
            viewModel.FormIntro = CurrentPage.GetPropertyValue<string>(ContactDoctype.Fields.FormIntro);
        }
    }
}