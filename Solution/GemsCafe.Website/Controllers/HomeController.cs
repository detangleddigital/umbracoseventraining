﻿namespace GemsCafe.Website.Controllers
{
    using System.Web.Mvc;
    using DevTrends.MvcDonutCaching;
    using GemsCafe.Website.App;
    using GemsCafe.Website.App.CmsDefinitions;
    using GemsCafe.Website.ViewModels;
    using Umbraco.Web;
    using Umbraco.Web.Models;

    public class HomeController : BaseController
    {
        [DonutOutputCache(CacheProfile = CacheProfiles.SixHours)]
        [HttpGet]
        public override ActionResult Index(RenderModel model)
        {
            var viewModel = CreateViewModel<HomeViewModel>();

            viewModel.TitleIntro = CurrentPage.GetPropertyValue<string>(HomeDoctype.Fields.TitleIntro);
            viewModel.Title = CurrentPage.GetPropertyValue<string>(HomeDoctype.Fields.Title);
            viewModel.TagLine = CurrentPage.GetPropertyValue<string>(HomeDoctype.Fields.TagLine);

            viewModel.Content1Title = CurrentPage.GetPropertyValue<string>(HomeDoctype.Fields.Content1Title);
            viewModel.Content1Text = CurrentPage.GetPropertyValue<string>(HomeDoctype.Fields.Content1Text);
            viewModel.Content2Title = CurrentPage.GetPropertyValue<string>(HomeDoctype.Fields.Content2Title);
            viewModel.Content2Text = CurrentPage.GetPropertyValue<string>(HomeDoctype.Fields.Content2Text);

            return CurrentTemplate(viewModel);
        }
    }
}